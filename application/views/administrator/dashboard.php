<div class="container-fluid">
<div class="alert alert-success" role="alert">
<i class="fas fa-tachometer-alt"> Anda sedang di dalam Dashboard </i> 
</div>
<div class="alert alert-success" role="alert">
  <h4 class="alert-heading"> Selamat Datang </h4>
  <p>Selamat datang <strong> <?php echo $username;?> </strong> di Sistem Akademik Universitas Aji, Anda login sebagai <strong> <?php echo $level;?> </strong></p>
  <hr>
  <!-- Button trigger modal -->
<button type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModal">
<i class="fas fa-cogs"> Control Panel </i>
</button>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><i class="fas fa-cogs"> Control Panel </i></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <div class="row text-info text-center">
          <div class="col-md-3">
          <a href="<?php echo base_url(); ?>"><p class="nav-link small text-info">Mahasiswa</p></a><br>
          <i class="fas fa-3x fa-user-graduate"></i>
          </div>
          <div class="col-md-3">
          <a href="<?php echo base_url(); ?>"><p class="nav-link small text-info">Jurusan</p></a><br>
          <i class="fas fa-3x fa-pen"></i>
          </div>
          <div class="col-md-3">
          <a href="<?php echo base_url(); ?>"><p class="nav-link small text-info">Program Studi</p></a>
          <i class="fas fa-3x fa-book-reader"></i>
          </div>
          <div class="col-md-3">
          <a href="<?php echo base_url(); ?>"><p class="nav-link small text-info">Tahun Akademik</p></a>
          <i class="fas fa-3x fa-calendar-alt"></i>
          </div>
        </div><hr> 
        <div class="row text-info text-center">
          <div class="col-md-3">
          <a href="<?php echo base_url(); ?>"><p class="nav-link small text-info">Mata Kuliah</p></a>
          <i class="fas fa-3x fa-clipboard"></i>
          </div>
          <div class="col-md-3">
          <a href="<?php echo base_url(); ?>"><p class="nav-link small text-info">Dosen</p></a><br>
          <i class="fas fa-3x fa-chalkboard-teacher"></i>
          </div>
          <div class="col-md-3">
          <a href="<?php echo base_url(); ?>"><p class="nav-link small text-info">Input Nilai</p></a><br>
          <i class="fas fa-3x fa-sort-numeric-down"></i>
          </div>
          <div class="col-md-3">
          <a href="<?php echo base_url(); ?>"><p class="nav-link small text-info">KRS</p></a><br>
          <i class="fas fa-3x fa-edit"></i>
          </div>
        </div><hr>
        <div class="row text-info text-center">
          <div class="col-md-3">
          <a href="<?php echo base_url(); ?>"><p class="nav-link small text-info">KHS</p></a><br>
          <i class="fas fa-3x fa-file-alt"></i>
          </div>
          <div class="col-md-3">
          <a href="<?php echo base_url(); ?>"><p class="nav-link small text-info">Cetak Transkip</p></a>
          <i class="fas fa-3x fa-print"></i>
          </div>
        </div><hr>
        <div class="row text-info text-center">
          <div class="col-md-3">
          <a href="<?php echo base_url(); ?>"><p class="nav-link small text-info">Identitas</p></a><br>
          <i class="fas fa-3x fa-id-card-alt"></i>
          </div>
          <div class="col-md-3">
          <a href="<?php echo base_url(); ?>"><p class="nav-link small text-info">Kategori</p></a><br>
          <i class="fas fa-3x fa-list-ul"></i>
          </div>
          <div class="col-md-3">
          <a href="<?php echo base_url(); ?>"><p class="nav-link small text-info">Informasi Kampus</p></a>
          <i class="fas fa-3x fa-bullhorn"></i>
          </div>
          <div class="col-md-3">
          <a href="<?php echo base_url(); ?>"><p class="nav-link small text-info">Tentang Kampus</p></a>
          <i class="fas fa-3x fa-info-circle"></i>
          </div>
        </div><hr>
        <div class="row text-info text-center">
          <div class="col-md-3">
          <a href="<?php echo base_url(); ?>"><p class="nav-link small text-info">Fasilitas</p></a><br>
          <i class="fas fa-3x fa-laptop"></i>
          </div>
          <div class="col-md-3">
          <a href="<?php echo base_url(); ?>"><p class="nav-link small text-info">Materi Kuliah</p></a>
          <i class="fas fa-3x fa-bookmark"></i>
          </div>
          <div class="col-md-3">
          <a href="<?php echo base_url(); ?>"><p class="nav-link small text-info">Galery</p></a><br>
          <i class="fas fa-3x fa-images"></i>
          </div>
          <div class="col-md-3">
          <a href="<?php echo base_url(); ?>"><p class="nav-link small text-info">Kontak</p></a><br>
          <i class="fas fa-3x fa-address-book"></i>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
</div>
